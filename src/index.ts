/*
 * @Author: Season
 * @Date: 2020-07-29 17:42:37
 * @LastEditTime: 2020-08-12 11:28:45
 * @FilePath: \blog-chain\src\index.ts
 */
// import Block from './block';

import * as WebSocket from 'ws';
import BlockChain from './blockChain';
import { initWebsocket, initMessageHandler } from './websocket';
import initHttp from './httpServe';

const HTTP_PORT = process.env.HTTP_PORT || 3001;
const P2P_PORT = process.env.P2P_PORT || 6001;

const newBlockChain = new BlockChain();

// websocket
const sockets = initWebsocket(Number(P2P_PORT));
if (Number(P2P_PORT) === 6002) {
  const ws = new WebSocket('ws://localhost:6001');
  ws.on('open', () => {
    initMessageHandler(ws);
    // ws.send('6002 connect 6001');
  });
}

// http
initHttp(Number(HTTP_PORT), sockets, newBlockChain);
