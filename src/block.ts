/*
 * @Author: Season
 * @Date: 2020-08-04 15:59:43
 * @LastEditTime: 2020-08-06 14:58:58
 * @FilePath: \blog-chain\src\block.ts
 */
import * as crypto from 'crypto';

export default class Block {
  private version = 'v1';

  private Nonce = crypto.randomBytes(32).toString('hex')

  index = 0;

  prevHash = '';

  time = 0;

  data = '';

  hash = '';

  constructor(index: number, prevHash: string, time: number, data: string) {
    this.index = index;
    this.prevHash = prevHash;
    this.time = time;
    this.data = data;
    this.hash = crypto
      .createHash('sha256')
      .update(index + prevHash + time + data)
      .digest('hex');
  }
}
